﻿using Microsoft.Extensions.Configuration;

namespace MonkeyAsylum.MovieManager.Data.Utility {
	public class DbOptions {

		public string ConnectionString {
			get; set;
		}

		public string DatabaseName {
			get; set;
		}

		public string MovieCollectionName {
			get; set;
		} = "movies";

		public DbOptions() {

		}

		public static DbOptions Settings { get; } = new DbOptions();

		public static void BindConfig(IConfiguration configuration) {
			configuration.GetSection("MonkeyAsylum:MovieManager:Data:Utility:DbOptions").Bind(Settings);
		}

	}
}
