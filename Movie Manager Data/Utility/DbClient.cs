﻿using MongoDB.Driver;

namespace MonkeyAsylum.MovieManager.Data.Utility {
	internal class DbClient {

		internal readonly static DbClient _DbConnection = new DbClient();

		private MongoClient _client {
			get; set;
		}

		private IMongoDatabase _database {
			get; set;
		}

		private DbClient() {
			_client = new MongoClient(DbOptions.Settings.ConnectionString);
			_database = _client.GetDatabase(DbOptions.Settings.DatabaseName);
		}

		internal IMongoCollection<T> GetCollection<T>(string collectionName) {
			return _database.GetCollection<T>(collectionName);
		}

	}
}
