﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MonkeyAsylum.MovieManager.Data.Models {
	
	[BsonIgnoreExtraElements]
	public class Movie {

		[BsonId]
		[BsonIgnoreIfDefault]
		[BsonIgnoreIfNull]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id {
			get; set;
		}

		[Required(AllowEmptyStrings = false)]
		public string Title {
			get; set;
		}

		[Required(AllowEmptyStrings = false)]
		//[Range(typeof(DateTime), "1900-01-01", "2100-01-01")]
		[Display(Prompt = "yyyy-MM-dd")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}", ConvertEmptyStringToNull = true)]
		[BsonIgnoreIfDefault]
		[BsonIgnoreIfNull]
		[BsonRepresentation(BsonType.DateTime)]
		public DateTime? ReleaseDate {
			get;set;
		}

		[BsonIgnore]
		public int ReleaseYear {
			get {
				return ReleaseDate?.Year ?? 0;
			}
			set {
				ReleaseDate = new DateTime(value, ReleaseDate?.Month ?? 1, ReleaseDate?.Day ?? 1);
			}
		}

		//[DisplayFormat(DataFormatString = "HH:mm", ApplyFormatInEditMode = true)]
		[BsonIgnoreIfDefault]
		public TimeSpan Length {
			get; set;
		}

		[BsonIgnoreIfNull]
		public AudienceRating Rating {
			get; set;
		}

		public string GetThumbnail() {
			return "movie-placeholder.png";
		}
	}
}
