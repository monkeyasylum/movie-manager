﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Operations;
using MonkeyAsylum.MovieManager.Data.Utility;
using ReturnDocument = MongoDB.Driver.ReturnDocument;

namespace MonkeyAsylum.MovieManager.Data.Models {
	public class MovieCollection : ICollection<Movie> {

		private readonly IMongoCollection<Movie> _movies;

		public int Count {
			get {
				return (int)(_movies?.CountDocuments(FilterDefinition<Movie>.Empty) ?? 0);
			}
		}

		public bool IsReadOnly {
			get {
				return false;
			}
		}

		public Movie this[ObjectId id] {
			get {
				return _movies.Find((m) => m.Id.Equals(id)).FirstOrDefault();
			}
			set {
				_movies.FindOneAndReplace<Movie>((m) => m.Id.Equals(id), value, new FindOneAndReplaceOptions<Movie>() {
					IsUpsert = true,
					ReturnDocument = ReturnDocument.After
				});
			}
		}

		public Movie this[string id] {
			get {
				ObjectId testId;
				try {
					testId = new ObjectId(id);
				}
				catch (IndexOutOfRangeException) {
					return null;
				}
				return _movies.Find((m) => m.Id.Equals(testId)).FirstOrDefault();
			}
			set {
				ObjectId testId;
				try {
					testId = new ObjectId(id);
				}
				catch (IndexOutOfRangeException) {
					testId = new ObjectId();
				}
				value.Id = _movies.FindOneAndReplace<Movie>((m) => m.Id.Equals(testId), value, new FindOneAndReplaceOptions<Movie>() {
					IsUpsert = true,
					ReturnDocument = ReturnDocument.After
				}).Id;
			}
		}

		public MovieCollection() {
			_movies = DbClient._DbConnection.GetCollection<Movie>(DbOptions.Settings.MovieCollectionName);
		}

		public void Add(Movie item) {
			_movies.InsertOne(item);
		}

		public bool Update(Movie item) {
			Movie result = _movies.FindOneAndUpdate<Movie>((m) => m.Id.Equals(item.Id), new ObjectUpdateDefinition<Movie>(item), new FindOneAndUpdateOptions<Movie, Movie>() { IsUpsert = true, ReturnDocument = ReturnDocument.After });
			item.Id = result?.Id ?? item.Id;
			return result != null;
		}

		public void Clear() {
			_movies?.DeleteMany(FilterDefinition<Movie>.Empty);
		}

		public bool Contains(Movie item) {
			return _movies?.Find(new ObjectFilterDefinition<Movie>(item))?.Any() ?? false;
		}

		public bool Contains(ObjectId id) {
			return _movies?.Find<Movie>((m) => m.Id.Equals(id))?.Any() ?? false;
		}

		public bool Contains(string id) {
			ObjectId testId;
			try {
				testId = new ObjectId(id);
			} catch (IndexOutOfRangeException) {
				return false;
			}
			return _movies?.Find<Movie>((m) => m.Id.Equals(testId))?.Any() ?? false;
		}

		public void CopyTo(Movie[] array, int arrayIndex) {
			foreach (Movie m in _movies.AsQueryable().AsEnumerable()) {
				array[arrayIndex++] = m;
			}
		}

		IEnumerator<Movie> IEnumerable<Movie>.GetEnumerator() {
			return _movies?.AsQueryable<Movie>().GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return _movies?.AsQueryable().GetEnumerator();
		}

		public bool Remove(Movie item) {
			return _movies?.DeleteOne(new ObjectFilterDefinition<Movie>(item)).IsAcknowledged ?? true;
		}

		public bool Remove(ObjectId id) {
			return _movies?.DeleteOne<Movie>((m) => m.Id.Equals(id)).IsAcknowledged ?? true;
		}

		public bool Remove(string id) {
			ObjectId testId;
			try {
				testId = new ObjectId(id);
			}
			catch (IndexOutOfRangeException) {
				return true;
			}
			return _movies?.DeleteOne<Movie>((m) => m.Id.Equals(testId)).IsAcknowledged ?? true;
		}
	}
}
