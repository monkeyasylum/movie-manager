﻿namespace MonkeyAsylum.MovieManager.Data.Models {
	public class AudienceRating {

		public static readonly AudienceRating General = new AudienceRating { Code = "G", Name = "General" };
		public static readonly AudienceRating Teen = new AudienceRating { Code = "T", Name = "Teen" };
		public static readonly AudienceRating PG13 = new AudienceRating { Code = "PG13", Name = "Parental Guidance 13" };
		public static readonly AudienceRating Mature = new AudienceRating { Code = "M", Name = "Mature" };
		public static readonly AudienceRating Restricted = new AudienceRating { Code = "R", Name = "Restricted" };
		public static readonly AudienceRating Adult = new AudienceRating { Code = "X", Name = "Adult" };
		public static readonly AudienceRating Unrated = new AudienceRating { Code = "U", Name = "Unrated" };

		public string Code {
			get; set;
		}

		public string Name {
			get; set;
		}
	}
}