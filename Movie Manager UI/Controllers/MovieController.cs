﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.Operations;
using MonkeyAsylum.MovieManager.Data.Models;
using MonkeyAsylum.MovieManager.UI.Configuration;
using MonkeyAsylum.MovieManager.UI.Models;
using MonkeyAsylum.MovieManager.UI.Models.Tmdb;
using RestSharp;

namespace MonkeyAsylum.MovieManager.UI.Controllers {
	public class MovieController : Controller {

		private readonly MovieCollection _movies = new MovieCollection();

		public ActionResult Default() {
			ViewData["Title"] = "Thumbnail View";
			ViewData["ReturnUrl"] = "_MovieThumbnails";
			return View(_movies);
		}

		public ActionResult ThumbnailView() {
			ViewData["Title"] = "Thumbnail View";
			ViewData["ReturnUrl"] = "_MovieThumbnails";
			return PartialView("_MovieThumbnails", _movies);
		}

		public ActionResult ListView() {
			ViewData["Title"] = "List View";
			ViewData["ReturnUrl"] = "_MovieList";
			return PartialView("_MovieList", _movies);
		}

		public ActionResult DetailListView() {
			ViewData["Title"] = "Details View";
			ViewData["ReturnUrl"] = "_MovieDetailList";
			return PartialView("_MovieDetailList", _movies);
		}

		public ActionResult ViewMovie(string id) {
			Movie target = _movies[id];
			ViewData["Mode"] = "EditMovie";
			ViewBag.AllRatings = GenerateRatingList(target.Rating);
			return PartialView("_MovieView", target);
		}

		[HttpGet]
		public ActionResult AddMovie() {
			Movie target = new Movie();
			ViewData["Mode"] = "AddMovie";
			ViewBag.AllRatings = GenerateRatingList();
			return PartialView("_MovieView", target);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AddMovie(Movie target, string returnUrl) {
			try {
				if (target.Rating?.Code == null || target.Rating?.Name == null) {
					target.Rating = null;
				}

				if (ModelState.IsValid) {
					_movies.Add(target);
				}
				else {
					ViewData["Mode"] = "AddMovie";
					ViewData["IsInvalid"] = true;
					ViewData["BodyOnly"] = true;
					ViewBag.AllRatings = GenerateRatingList(target.Rating);
					return PartialView("_MovieView", target);
				}
			}
			catch {
				ViewData["Mode"] = "AddMovie";
				ViewData["IsInvalid"] = true;
				ViewData["BodyOnly"] = true;
				ViewBag.AllRatings = GenerateRatingList(target.Rating);
				return PartialView("_MovieView", target);
			}
			ViewData["ReturnUrl"] = returnUrl;
			return PartialView(returnUrl, _movies);
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditMovie(Movie target, string returnUrl) {
			try {
				if (target.Rating?.Code == null || target.Rating?.Name == null) {
					target.Rating = null;
				}

				if (ModelState.IsValid) {
					_movies.Update(target);
				}
				else {
					ViewData["Mode"] = "EditMovie";
					ViewData["IsInvalid"] = true;
					ViewData["BodyOnly"] = true;
					ViewBag.AllRatings = GenerateRatingList(target.Rating);
					return PartialView("_MovieView", target);
				}
			}
			catch {
				ViewData["Mode"] = "EditMovie";
				ViewData["IsInvalid"] = true;
				ViewData["BodyOnly"] = true;
				ViewBag.AllRatings = GenerateRatingList(target.Rating);
				return PartialView("_MovieView", target);
			}
			ViewData["ReturnUrl"] = returnUrl;
			return PartialView(returnUrl, _movies);
		}

		public ActionResult SearchMovie(string title, DateTime? releaseDate) {
			RestClient tmdbClient = new RestClient($"{TmdbOptions.Settings.BaseUrl}");
			tmdbClient.AddDefaultHeader("Authorization", $"Bearer {TmdbOptions.Settings.BearerToken}");
			//configuration
			if (TmdbConfiguration.Settings == null) {
				IRestResponse<TmdbConfiguration> configurationResponse = tmdbClient.Execute<TmdbConfiguration>(new RestRequest($"configuration"));
				if (configurationResponse.StatusCode == System.Net.HttpStatusCode.OK) {
					TmdbConfiguration.Settings = configurationResponse.Data;
				}
			}
			//search/movie?language=en-CA&include_adult=false&region=CA&query=Red%2BDawn&year=1984
			//search/movie?language=en-CA&query=rambo&page=2&include_adult=true&region=CA&year=1980
			IRestResponse<Models.Tmdb.SearchResults> response = tmdbClient.Execute<Models.Tmdb.SearchResults>(new RestRequest($"search/movie?language=en-CA&query={title}&region=CA&year={releaseDate?.Year}"));
			SearchResults results = new SearchResults();
			if (response.StatusCode == System.Net.HttpStatusCode.OK) {
				results = response.Data;
				while (results.Page < results.TotalPages) {
					response = tmdbClient.Execute<Models.Tmdb.SearchResults>(new RestRequest($"search/movie?language=en-CA&query={title}&page={++results.Page}&region=CA&year={releaseDate?.Year}"));
					if (response.StatusCode == System.Net.HttpStatusCode.OK) {
						results.Results.AddRange(response.Data.Results);
					}
					else {
						//TODO - handle error
					}
				}
			}
			else {
				//TODO - handle error
			}
			return PartialView("_SearchResults", results);
		}

		[HttpPost]
		public ActionResult Delete(string id, string returnUrl) {
			try {
				bool result = _movies.Remove(id);
				if (!result) {
					Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					return new JsonResult(new {
						message = $"Delete Failed!"
					});
				}
			}
			catch(Exception ex) {
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return new JsonResult(new {
					Message = $"{ex.Message}"
				});
			}
			ViewData["ReturnUrl"] = returnUrl;
			return PartialView(returnUrl, _movies);
		}

		public class AudienceRatingSelectItem {
			public string Display {
				get; private set;
			}
			public string Value {
				get; private set;
			}

			public AudienceRatingSelectItem(string noitemtext) {
				Display = noitemtext;
				Value = null;
			}

			public AudienceRatingSelectItem(AudienceRating item) {
				Display = item.Name;
				Value = item.Code;
			}
		}

		private SelectList GenerateRatingList(AudienceRating selectedItem=null) {
			//var test = new SelectListItem()
			List<AudienceRatingSelectItem> values = new List<AudienceRatingSelectItem>() {
				new AudienceRatingSelectItem("Not Selected"),
				new AudienceRatingSelectItem(AudienceRating.General),
				new AudienceRatingSelectItem(AudienceRating.Teen),
				new AudienceRatingSelectItem(AudienceRating.PG13),
				new AudienceRatingSelectItem(AudienceRating.Mature),
				new AudienceRatingSelectItem(AudienceRating.Restricted),
				new AudienceRatingSelectItem(AudienceRating.Adult),
				new AudienceRatingSelectItem(AudienceRating.Unrated)
			};
			return new SelectList(values, "Value", "Display", values.FirstOrDefault((i)=>i.Value == selectedItem?.Code));
		}
	}
}