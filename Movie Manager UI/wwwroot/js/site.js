﻿function enablepops(rootid) {
	$(rootid).find('[data-toggle="tooltip"]').tooltip();
	$(rootid).find('[data-toggle="popover"]').popover();
}

function movieSubmit(sender) {
	var form = $(sender).parents('.modal').find('form');
	var dialog = $(sender).parents('.modal');
	var returnUrl = $(dialog).siblings('#mainContent').find('[name=ReturnUrl]').val();
	var actionUrl = form.attr('action');
	var dataToSend = form.serialize() + '&returnUrl=' + returnUrl;

	if (form.valid()) {
		$.post(actionUrl, dataToSend)
			.done(function (data) {
				var isInvalid = (Boolean)($('[name="IsInvalid"]', data).val());

				if (isInvalid) {
					dialog.find("#movieViewContent").find('.modal-body').replaceWith(data);
				} else {
					dialog.find("#movieViewContent").html("");
					dialog.modal('hide');
					$('#mainContent').html(data);
				}
			})
			.fail(function (data) {
				alert("Status Code: " + data.status + "\n" + data.statusText + "\n" + data.responseText);
			})
			;
	}
}

function movieDelete(sender) {
	var form = $(sender).parents('.modal').find('form');
	var dialog = $(sender).parents('.modal');
	var returnUrl = $(dialog).siblings('#mainContent').find('[name=ReturnUrl]').val();
	var dataToSend = "id=" + $(dialog).find('#IdField').val() + '&returnUrl=' + returnUrl;

	if (confirm("Are you sure you want to delete '" + $(dialog).find('#TitleField').val() + "'?")) {
		$.post("Movie/Delete", dataToSend)
			.done(function (data) {
				dialog.find("#movieViewContent").html("");
				dialog.modal('hide');
				$('#mainContent').html(data);
			})
			.fail(function (data) {
				if (data.responseJSON != undefined) {
					alert(data.responseJSON.message);
				} else {
					alert("Status Code: " + data.status + "\n" + data.statusText + "\n" + data.responseText);
				}
			})
			;
	}
}

function movieSearch(sender) {
	var form = $(sender).parents('.modal').find('form');
	var resultsArea = $(sender).parents("#movieViewContent").find("#searchResults");
	$(resultsArea).text("Searching...");
	var dataToSend = "title=" + $(form).find('#TitleField').val() + "&releaseDate=" + $(form).find('#ReleaseDateField').val();

	$.get("Movie/SearchMovie", dataToSend)
		.done(function (data) {
			$(resultsArea).html(data);
		})
		.fail(function (data) {
			alert("status code: " + data.status + "\n" + data.statustext + "\n" + data.responsetext);
		})
		;
}

function applyTmdbData(sender, fieldId, value) {
	var form = $(sender).parents('.modal').find('form');
	$(form).find(fieldId).val(value);
}

function updateRating(sender) {
	var nameField = $(sender).parent().find('#RatingName');
	var name = sender.options[sender.options.selectedIndex].text;
	$(nameField).val(name);
}
