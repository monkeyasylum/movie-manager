using System;

namespace MonkeyAsylum.MovieManager.UI.Models {
	public class ErrorViewModel {
		public string RequestId {
			get; set;
		}

		public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
	}
}
