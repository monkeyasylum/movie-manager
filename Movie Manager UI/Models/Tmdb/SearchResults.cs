﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonkeyAsylum.MovieManager.UI.Models.Tmdb {
	public class SearchResults {

		public int Page {
			get; set;
		}

		public List<SearchResult> Results {
			get; set;
		}

		public int TotalResults {
			get; set;
		}
		public int TotalPages {
			get; set;
		}
	}
}
