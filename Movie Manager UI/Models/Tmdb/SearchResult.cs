﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MonkeyAsylum.MovieManager.UI.Configuration;

namespace MonkeyAsylum.MovieManager.UI.Models.Tmdb {
	public class SearchResult {

		public int Id {
			get; set;
		}

		public string Title {
			get; set;
		}

		public string PosterPath {
			get; set;
		}

		[Display(Prompt = "yyyy-MM-dd")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}", ConvertEmptyStringToNull = true)]
		public DateTime ReleaseDate {
			get; set;
		}

		public string Overview {
			get; set;
		}

		public List<int> GenreIds {
			get; set;
		}
		//public bool Adult{get;set;}
		//public string OriginalTitle{get;set;}
		//public string OriginalLanguage{get;set;}
		//public string BackdropPath{get;set;}
		//public number Popularity{get;set;}
		//public int VoteCount{get;set;}
		//public bool Video{get;set;}
		//public number VoteAverage{get;set;}

		public string GetPosterUrl() {
			string Url = TmdbConfiguration.Settings.Images.SecureBaseUrl ?? TmdbConfiguration.Settings.Images.BaseUrl;
			return string.IsNullOrWhiteSpace(Url) || string.IsNullOrWhiteSpace(PosterPath) ? "/cache/movie-placeholder.png" : $"{Url}{TmdbConfiguration.Settings.GetPosterSizeString(500)}{PosterPath}";
		}
	}
}
