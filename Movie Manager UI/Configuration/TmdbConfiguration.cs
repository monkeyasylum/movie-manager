﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MonkeyAsylum.MovieManager.UI.Configuration {
	public class TmdbConfiguration {

		public class ImageConfiguration {

			[JsonPropertyName("base_url")]
			public string BaseUrl {
				get; set;
			}

			[JsonPropertyName("secure_base_url")]
			public string SecureBaseUrl {
				get; set;
			}

			[JsonPropertyName("backdrop_sizes")]
			public List<string> BackdropSizes {
				get; set;
			}

			[JsonPropertyName("logo_sizes")]
			public List<string> LogoSizes {
				get; set;
			}

			[JsonPropertyName("poster_sizes")]
			public List<string> PosterSizes {
				get; set;
			}

			[JsonPropertyName("profile_sizes")]
			public List<string> ProfileSizes {
				get; set;
			}

			[JsonPropertyName("still_sizes")]
			public List<string> StillSizes {
				get; set;
			}

			internal int SizeStringToInt(string value) {
				Regex widthRegex = new Regex("w([0-9]+)");
				GroupCollection result = widthRegex.Match(value).Groups;
				return result.Count > 1 ? int.Parse(result[1].Value) : 0;
			}
		}

		public static TmdbConfiguration Settings {
			get; internal set;
		}

		[JsonPropertyName("images")]
		public ImageConfiguration Images {
			get; set;
		}

		[JsonPropertyName("change_keys")]
		public List<string> ChangeKeys {
			get; set;
		}

		public string GetPosterSizeString(int width) {
			return Images.PosterSizes.OrderByDescending(Images.SizeStringToInt).FirstOrDefault((s) => Images.SizeStringToInt(s) <= width);
		}
	}
}
