﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace MonkeyAsylum.MovieManager.UI.Configuration {
	public class TmdbOptions {

		public string BaseUrl {
			get; set;
		}
		
		public string BearerToken {
			get; set;
		}

		public static TmdbOptions Settings { get; } = new TmdbOptions();

		public static void BindConfig(IConfiguration configuration) {
			configuration.GetSection("MonkeyAsylum:MovieManager:Tmdb").Bind(Settings);
		}
	}
}
