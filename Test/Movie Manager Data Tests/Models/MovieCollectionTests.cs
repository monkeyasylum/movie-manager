﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestPlatform.CrossPlatEngine.Discovery;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MonkeyAsylum.MovieManager.Data.Models;
using MonkeyAsylum.MovieManager.Data.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MonkeyAsylum.MovieManager.Data.Models.Tests {
	[TestClass()]
	public class MovieCollectionTests {

		private readonly Random _rng = new Random();
		private MovieCollection _testCollection;

		[TestInitialize]
		public void Init() {
			var config = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json")
				.Build();
			DbOptions.BindConfig(config);
			_testCollection = new MovieCollection();
		}

		[TestMethod()]
		public void AddTest() {
			AudienceRating newRating = null;
			switch (_rng.Next(0, 10)) {
				case 0:
					newRating = AudienceRating.General;
					break;
				case 1:
					newRating = AudienceRating.Teen;
					break;
				case 2:
					newRating = AudienceRating.PG13;
					break;
				case 3:
					newRating = AudienceRating.Mature;
					break;
				case 4:
					newRating = AudienceRating.Restricted;
					break;
				case 5:
					newRating = AudienceRating.Adult;
					break;
				case 6:
					newRating = AudienceRating.Unrated;
					break;
				default:
					newRating = null;
					break;
			}
			Movie testMovie = new Movie() {
				Title = $"Movie {_rng.Next(1, 10000):0000}",
				ReleaseYear = _rng.Next(1950, 2021),
				Length = new TimeSpan(_rng.Next(0, 3), _rng.Next(0, 61), _rng.Next(0, 61)),
				Rating = newRating
			};
			Trace.WriteLine($"Adding {testMovie.Id}: {testMovie.Title}");
			_testCollection.Add(testMovie);
			Assert.IsTrue(_testCollection.Contains(testMovie));
			Trace.WriteLine($"Added {testMovie.Id}: {testMovie.Title}");
		}

		[TestMethod()]
		public void UpdateTest() {
			Movie target = _testCollection.AsEnumerable().Skip(_rng.Next(0, _testCollection.Count)).First();
			Trace.WriteLine($"UpdateTest: Id: {target.Id} Original Title: {target.Title}");
			target.Title = $"Movie {_rng.Next(1, 10000):0000}";
			Trace.WriteLine($"UpdateTest: Id: {target.Id} New Title: {target.Title}");
			Assert.IsTrue(_testCollection.Update(target));
			target.Id = new MongoDB.Bson.ObjectId().ToString();
			target.Title = $"Movie {_rng.Next(1, 10000):0000}";
			Trace.WriteLine($"UpdateTest: Id: {target.Id} New Title: {target.Title}");
			Assert.IsTrue(_testCollection.Update(target));
			Trace.WriteLine($"UpdateTest: Id: {target.Id} New Title: {target.Title}");
		}

		[TestMethod()]
		public void ContainsTest() {
			Movie target = _testCollection.AsEnumerable().Skip(_rng.Next(0, _testCollection.Count)).First();
			Assert.IsFalse(_testCollection.Contains("FA17"));
			Assert.IsFalse(_testCollection.Contains("000000000000000000000000"));
			Assert.IsTrue(_testCollection.Contains(target.Id.ToString()));
			Assert.IsFalse(_testCollection.Contains(new ObjectId()));
			Assert.IsTrue(_testCollection.Contains(target.Id));
			Assert.IsFalse(_testCollection.Contains(new Movie()));
			Assert.IsTrue(_testCollection.Contains(target));
		}

		[TestMethod()]
		public void RemoveTest() {
			Movie target = _testCollection.AsEnumerable().Skip(_rng.Next(0, _testCollection.Count)).First();
			Assert.IsTrue(_testCollection.Remove("FA17"));
			Assert.IsTrue(_testCollection.Remove("000000000000000000000000"));
			Assert.IsTrue(_testCollection.Remove(new ObjectId()));
			Assert.IsTrue(_testCollection.Remove(new Movie()));
			Assert.IsTrue(_testCollection.Remove(target.Id.ToString()));
			Assert.IsFalse(_testCollection.Contains(target.Id.ToString()));
			_testCollection.Add(target);
			Assert.IsTrue(_testCollection.Remove(target.Id));
			Assert.IsFalse(_testCollection.Contains(target.Id));
			_testCollection.Add(target);
			Assert.IsTrue(_testCollection.Remove(target));
			Assert.IsFalse(_testCollection.Contains(target));
			_testCollection.Add(target);
		}
	}
}